const axios = require('axios');
const API_URL = 'https://c8722e46832b.ngrok.io';
// const {SocrateService} = require('@socrate/client');
const MemoryStorage = require('memorystorage');


let storage = new MemoryStorage('my-app');
console.log(storage);
function createUser() {
  const uniqueId = (Math.random() * 100).toFixed(0);
  const userData= {
    validatorId: uniqueId,
    email: `test${uniqueId}@gmail.com`,
    password: 'toto',
    firstName: 'Toto',
    lastName: 'Toto',
    birthday: 971388000000,
    gender: 'male'
  };

  return axios.post(`${API_URL}/users/`, userData)
    .then(result => console.log(JSON.stringify(result, null, 2)))
    .catch(err => console.log(err));
}

let count = parseFloat(process.argv[2]);
console.log(count);
return createUser()
  .then(() => {
    count -= 1;
    console.log(`Count : ${count}`);
    if (count)
      return createUser();
  }).then(() => console.log('Done.'));
