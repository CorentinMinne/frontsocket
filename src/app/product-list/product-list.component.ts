import {Component} from '@angular/core';

import {SocrateService} from '@socrate/client';


const url = 'https://d717477b966d.ngrok.io/';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent {
  constructor() {
    this.socrate = new SocrateService(url, {});
  }

  public socket;
  socrate: SocrateService;
  userId = null;
  chat = null;
  jwt = '';
  isWriting = false;
  timeout = undefined;
  selectedOption = 0;
  usersEmail = [
    'user1@test.fr',
    'user2@test.fr',
    'user3@test.fr',
    'user4@test.fr',
  ];

  ngOnInit(): void {
    this.socket = this.socrate.getSocket();
  }

  writing() {
    if (!this.isWriting) {
      this.socket.emit('typing', {userId: this.userId, isTyping: true, chatMembers: this.chat.members});
    }
    this.isWriting = true;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.isWriting = false;
      this.socket.emit('typing', {userId: this.userId, isTyping: false, chatMembers: this.chat.members});
    }, 3000);
  }

  // refresh() {
  //   const body = {
  //     strategy: 'refresh-token'
  //   };
  //   this.socrate.logIn(body).then((res) => {
  //     console.log('login refresh token ');
  //     console.log(res);
  //     this.jwt = res.accessToken;
  //   });
  //
  // }

  login() {
    const body = {
      email: this.usersEmail[this.selectedOption],
      password: 'toto',
      strategy: 'local',
      entity: 'users'
    };

    this.socrate.logIn(body).then((res) => {
      console.log('login local');
      console.log(res);
      this.userId = res.userId;
      this.jwt = res.accessToken;

      this.socrate.find('chats' , {
        members: {$in: [this.userId]}
      }).then(chats => this.chat = chats.data[0]);


      this.socket.on(this.userId, (message) => {
        if ('typing' in message) {
          if (message.typing) {
            console.log(message.userId, ' is typing.');
          }
          else {
            console.log(message.userId, 'stop typing.');
          }
        }
      });

    });
  }

  // get() {
  //   // this.socrate._verifyJWT(this.jwt).then((res) => {
  //   //   console.log("verify: ");
  //   //   console.log(res);
  //   // }).catch((error) => {
  //   //   console.log('error: ');
  //   //   console.log(error);
  //   //   const body = {
  //   //     strategy: 'refresh-token'
  //   //   };
  //   //   this.socrate.logIn(body).then((res) => {
  //   //     console.log("login refresh token ");
  //   //     console.log(res);
  //   //     this.jwt = res.accessToken;
  //   //   });
  //   // });
  //   const users = this.socrate.watch<any>('users', {}).subscribe((res) => {
  //     console.log('c\'est bon', res);
  //   } , err => console.log(err));
  //   // this.socrate.find<SocrateUser>('users', {});
  //   // this.http.get(url + 'users', {responseType: 'json', observe: 'response' as 'response', withCredentials: true,
  //   //   headers: {
  //   //     authorization: this.jwt
  //   //   }}).
  //   // subscribe((res: HttpResponse<any>) => {
  //   //   console.log('response from server:', res);
  //   //   console.log('response headers', res.headers.keys());
  //   // });
  //   console.log(users);
  //
  // }
  // register() {
  //   this.socrate.signUp({
  //     email: 'helloWorl34d@test.fr',
  //     firstName: 'Valentin',
  //     lastName: 'Montagne',
  //     password: 'toto',
  //     birthday: 20201002,
  //     gender: 'male'
  //   }).then((res) => {
  //     console.log(res);
  //   });
  // }

  create4Accounts() {
    this.socrate.create('users', [{
      email: 'user1@test.fr',
      firstName: 'Valentin',
      lastName: 'Montagne',
      password: 'toto',
      birthday: 20201002,
      gender: 'male'
    }, {
      email: 'user2@test.fr',
      firstName: 'a',
      lastName: 'a',
      password: 'toto',
      birthday: 20201002,
      gender: 'male'
    }, {
      email: 'user3@test.fr',
      firstName: 'b',
      lastName: 'b',
      password: 'toto',
      birthday: 20201002,
      gender: 'male'
    }, {
      email: 'user4@test.fr',
      firstName: 'c',
      lastName: 'c',
      password: 'toto',
      birthday: 20201002,
      gender: 'male'
    }]);
  }

  createChat() {
    this.socrate.find('users', {
      email: {$in: this.usersEmail}
    }).then(users => {
      this.socrate.create('chats', {
        name: 'team chat',
        members: users.data.map(user => user._id),
        createdBy: this.userId
      }).then(chat => {
        this.chat = chat;
        console.log('Chat created');
      });
    });
  }





  // registerGuest() {
  //   this.socrate.create('guests', {
  //     email: 'd@d.fr',
  //     firstName: 'Toto',
  //     birthday: 876693600000,
  //     lastName: 'Titi',
  //     address: 'Ici et là',
  //     storeAccountUserId: '6026add9d008e453aa0c9758'
  //   }).then((res) => {
  //     console.log(res);
  //     this.jwt = res.auth.accessToken;
  //   });
  // }
  // loginGuest() {
  //   const body = {
  //     email: 'd@d.fr',
  //     password: 'toto',
  //     entity: 'guest',
  //     strategy: 'local'
  //   };
  //   const httpOptions = {
  //     headers: new HttpHeaders({
  //       accept: 'application/json'
  //     }),
  //     credentials: 'same-origin',
  //     withCredentials: true,
  //     observe: 'response' as 'response'
  //   };
  //   this.socrate.logIn(body).then((res) => {
  //     console.log('login local');
  //     console.log(res);
  //     this.jwt = res.accessToken;
  //   });
  // }
  //
  // logout() {
  //   this.socrate.logOut();
  // }
  //
  // share() {
  //   this.socrate.signUp({
  //     email: 'helloWorl34d@test.fr',
  //     firstName: 'Valentin',
  //     lastName: 'Montagne',
  //     password: 'toto',
  //     birthday: 20201002,
  //     gender: 'male'
  //   });
  //
  //   // this.socrate.logIn(body);
  //   // this.http.post('http://localhost:3030/authentication/', body, httpOptions)
  //   //   .subscribe((res: HttpResponse<any>) => {
  //   //     console.log('response from server:', res);
  //   //     console.log('response headers', res.headers.keys());
  //   //   });
  // }
}


/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/
